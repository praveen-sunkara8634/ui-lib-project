from web_ui_commons import WebUICommons
import json

class AuthenticationPage(WebUICommons):
    def __init__(self):
        with open("./json_files/authentication_page.json") as auth:
            self.locators = json.load(auth)

    def authentication(self, username, password):
        self.send_text(self.locators, 'username_xpath', username)
        self.send_text(self.locators, 'password_xpath', password)
        self.click(self.locators, 'let_me_in_btn_xpath')


