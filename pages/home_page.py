from web_ui_commons import WebUICommons
import json
import pydash

class HomePage(WebUICommons):
    def __init__(self):
        with open("./json_files/initial_setup.json") as home:
            self.locators = json.load(home)

    def open_homepage(self, url):
        value = pydash.get(self.locators, url)
        self.open_url(value)



