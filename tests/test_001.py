from pages.home_page import HomePage
from pages.authentication_page import AuthenticationPage
from pages.dashboard_page import DashboardPage
from pages.signup_form_page import SignUpPage
import fake_data as fd

def test_authentication(module_level):
    hp = HomePage()
    hp.open_homepage("url")
    auth = AuthenticationPage()
    auth.authentication("dxpuser","Hu8#de101")

def test_registration(module_level):
    dp = DashboardPage()
    sup = SignUpPage()
    dp.click_signup_link()
    sup.complete_registration(fname=fd.fname, lname=fd.lname, email=fd.email, phone=fd.phone_number)
