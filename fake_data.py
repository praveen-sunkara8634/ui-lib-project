from faker import Faker

fake = Faker()
email = fake.email()
fname = fake.first_name()
lname = fake.last_name()
password = fake.password()
phone_number = fake.phone_number()