import sys
from selenium import webdriver

class WebDriverFactory():

    grid = ['localhost', '172.24.0.124', '172.24.0.125']

    def get_selenium_driver_path(self, browser='chrome'):
        """
        Function to get selenium driver path
        Download drivers from here:
            FireFox: https://github.com/mozilla/geckodriver/releases
            Chrome: https://chromedriver.storage.googleapis.com/index.html?path=76.0.3809.126/
            Command for starting dockerized selenium grid: docker run --rm -ti --name zalenium -p 4444:4444 -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/videos:/home/seluser/videos --privileged dosel/zalenium start
        :param browser:
        :return:
        """
        if 'linux' in sys.platform:
            # This is Linux System
            if browser == 'chrome':
                web_driver_path = './web_drivers/Linux/chromedriver'
            else:
                web_driver_path = './web_drivers/Linux/geckodriver'
        elif 'darwin' in sys.platform:
            # This is Mac System
            if browser == 'chrome':
                web_driver_path = './web_drivers/Darwin/chromedriver'
            else:
                web_driver_path = './web_drivers/Darwin/geckodriver'
        elif 'win' in sys.platform:
            # This is Windows System
            if browser == 'chrome':
                web_driver_path = './web_drivers/Windows/chromedriver.exe'
            else:
                web_driver_path = './web_drivers/Windows/geckodriver.exe'
        else:
            raise Exception("ERROR: UI Automation is supported only on Linux and Mac (as of now) !!")

        return web_driver_path

    def get_driver(self, browser='chrome', grid=False, remote_ip=grid[0], remote_port=4444):
        remote_server = f"http://{remote_ip}:{remote_port}/wd/hub"
        global driver
        if(browser.lower()=="chrome"):
            if grid == True:
                web_driver_options = webdriver.ChromeOptions()
                web_driver_options.add_argument("--no-sandbox")
                web_driver_options.add_argument("--privileged")
                driver = webdriver.Remote(command_executor=remote_server,
                                                   desired_capabilities=web_driver_options.to_capabilities())
            else:
                driver = webdriver.Chrome(executable_path=self.get_selenium_driver_path(browser.lower()))

        else:
            if grid == True:
                web_driver_options = webdriver.FirefoxOptions()
                driver = webdriver.Remote(command_executor=remote_server,
                                               desired_capabilities=web_driver_options.to_capabilities())
            else:
                driver = webdriver.Firefox(executable_path=self.get_selenium_driver_path(browser.lower()))

        return driver




